#!/bin/bash
set -euo pipefail

source .env

python $PROJECT_NAME/manage.py consumemessages &
python $PROJECT_NAME/manage.py makemigrations
python $PROJECT_NAME/manage.py migrate
python $PROJECT_NAME/manage.py runserver 0.0.0.0:$PROJECT_PORT
