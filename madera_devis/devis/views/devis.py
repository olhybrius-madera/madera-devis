import jwt
from django.http import HttpResponse
from django.template.loader import render_to_string
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from weasyprint import HTML

from devis.models import Devis
from devis.serializers import *


class DevisViewSet(viewsets.ModelViewSet):
    queryset = Devis.objects.all()
    keycloak_roles = {
        'GET': ['commercial', 'client'],
        'POST': ['commercial'],
        'PUT': ['client'],
    }

    def get_queryset(self):
        req = self.request
        auth_header = req.META.get('HTTP_AUTHORIZATION').split()
        token = auth_header[1] if len(auth_header) == 2 else auth_header[0]
        decoded = jwt.decode(token, algorithms=["RS256"], options={"verify_signature": False})
        user = decoded['sub']
        return Devis.objects.filter(Q(commercial=user) | Q(client__uuid_utilisateur=user))

    def get_serializer_class(self):
        if self.action == 'accepter':
            return DevisAccepteSerializer
        elif self.action == 'refuser':
            return DevisRefuseSerializer
        elif self.action == 'commenter':
            return DevisCommenteSerializer
        else:
            return DevisSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    @action(detail=True, methods=['PUT'])
    def accepter(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_object(), data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    @action(detail=True, methods=['PUT'])
    def refuser(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_object(), data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    @action(detail=True, methods=['GET'])
    def telecharger(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_object())
        template_path = 'devis_template.html'
        html = render_to_string(template_path,
                                {'devis': serializer.data,
                                 'client': self.get_object().client,
                                 'date': self.get_object().date_emission},
                                request)
        file = HTML(string=html).write_pdf()
        response = HttpResponse(file, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="Report.pdf"'
        return response
