from django.db import models


class LigneDevis(models.Model):
    devis = models.ForeignKey("devis.Devis", on_delete=models.CASCADE)
    gamme = models.ForeignKey("devis.Gamme", on_delete=models.CASCADE)
    module = models.ForeignKey("devis.Module", on_delete=models.CASCADE)
    quantite = models.FloatField()
