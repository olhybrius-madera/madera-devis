from django.db import models


class PieceJointe(models.Model):
    libelle = models.CharField(max_length=50)
    chemin = models.CharField(max_length=50)
    devis = models.ForeignKey("devis.Devis", on_delete=models.CASCADE)
