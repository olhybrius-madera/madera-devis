from django.db import models


class Version(models.Model):
    date = models.DateTimeField()
    supprimee = models.BooleanField(default=False)

    class Meta:
        abstract = True

