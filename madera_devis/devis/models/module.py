import decimal

from django.db import models
import uuid

from django.db.models import Q

from . import Composant, VersionComposant
from .unite import Unite
from .version import Version


class Module(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)


class VersionModule(Version):
    libelle = models.CharField(max_length=50)
    unite = models.CharField(choices=Unite.choices, max_length=50)
    module = models.ForeignKey(Module, on_delete=models.CASCADE)
    gamme = models.ForeignKey("devis.Gamme", on_delete=models.CASCADE)

    @property
    def prix(self):
        composition = self.module.composition_set\
            .filter(date__lte=self.date) \
            .order_by('composant__uuid', '-date')\
            .distinct('composant__uuid')
        prix = 0

        for composant in composition:
            quantite = composant.quantite
            prix_composant = VersionComposant.objects \
                .filter(Q(composant=composant.composant) & Q(date__lte=self.date)) \
                .latest('date').prix

            prix += prix_composant * decimal.Decimal(quantite)

        return prix


class Composition(Version):
    module = models.ForeignKey(Module, on_delete=models.CASCADE)
    composant = models.ForeignKey(Composant, on_delete=models.CASCADE)
    quantite = models.FloatField()
