from django.db import models
import uuid

from .version import Version


class Gamme(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)


class VersionGamme(Version):
    class TypeIsolant(models.TextChoices):
        SYNTHETIQUE = 'SYNTHETIQUE'
        NATUREL = 'NATUREL'
        BIOLOGIQUE = 'BIOLOGIQUE'

    libelle = models.CharField(max_length=50)
    finition_exterieure = models.CharField(max_length=50)
    type_isolant = models.CharField(choices=TypeIsolant.choices, max_length=50)
    type_couverture = models.CharField(max_length=50)
    gamme = models.ForeignKey(Gamme, on_delete=models.CASCADE)
