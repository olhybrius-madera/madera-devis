import pika
from django.db import models
import uuid
import decimal
from django.conf import settings
from django.db.models import Q
from rest_framework.renderers import JSONRenderer

# importation du module complet pour éviter les références cycliques
import devis.models


class Devis(models.Model):
    class EtatDevis(models.TextChoices):
        BROUILLON = 'BROUILLON'
        TRANSFERT_FACTURATION = 'TRANSFERT_FACTURATION'
        EN_ATTENTE = 'EN_ATTENTE'
        ACCEPTE = 'ACCEPTE'
        REFUSE = 'REFUSE'
        EN_COMMANDE = 'EN_COMMANDE'

    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    commercial = models.UUIDField()
    reference_projet = models.CharField(max_length=50)
    nom_projet = models.CharField(max_length=50)
    date_emission = models.DateTimeField(auto_now_add=True, null=True)
    etat = models.CharField(choices=EtatDevis.choices, default=EtatDevis.BROUILLON, max_length=50)
    est_modele = models.BooleanField(default=False)
    commentaires = models.TextField()
    marge_commerciale = models.IntegerField(default=0.0)
    marge_entreprise = models.IntegerField(default=0.0)
    client = models.ForeignKey("devis.Client", on_delete=models.CASCADE)

    @property
    def gamme(self):
        ligne = self.lignedevis_set.first()

        if ligne is None:
            return ''

        gamme = ligne.gamme

        if self.date_emission is not None:
            version_gamme = devis.models.VersionGamme.objects\
                .filter(Q(gamme=gamme) & Q(date__lte=self.date_emission))\
                .latest('date')
        else:
            version_gamme = devis.models.VersionGamme.objects.filter(Q(gamme=gamme)).latest('date')

        return version_gamme

    @property
    def prix(self):
        qset = self.lignedevis_set.all()
        prix = 0
        for ligne in qset:
            if self.date_emission is not None:
                module = devis.models.VersionModule.objects \
                    .filter(Q(module=ligne.module) & Q(date__lte=self.date_emission)) \
                    .latest('date')
            else:
                module = devis.models.VersionModule.objects \
                    .filter(Q(module=ligne.module)) \
                    .latest('date')

            prix += module.prix * decimal.Decimal(ligne.quantite)

        return prix

    def accepter(self):
        # Importation du module localement pour éviter les références cycliques
        import devis.serializers

        self.etat = self.EtatDevis.ACCEPTE
        # Création d'un objet JSON contenant également les données issus des clés étrangères
        devis_json = JSONRenderer().render(devis.serializers.DevisSerializer(self).data)

        credentials = pika.PlainCredentials(settings.BROKER_USER, settings.BROKER_PASSWORD)
        parameters = pika.ConnectionParameters(settings.BROKER_HOST,
                                               settings.BROKER_PORT,
                                               settings.BROKER_VHOST,
                                               credentials)

        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.exchange_declare(exchange='madera', exchange_type='topic', durable=True)
        channel.basic_publish(exchange='madera', routing_key='devis.accepte',
                              body=devis_json)

    def refuser(self):
        self.etat = self.EtatDevis.REFUSE

    def commenter(self, commentaire):
        self.commentaires = commentaire
