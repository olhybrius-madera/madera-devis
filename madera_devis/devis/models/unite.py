from django.db import models


class Unite(models.TextChoices):
    CM = 'CM'
    PIECE = 'PIECE'
    M2 = 'M2'
    M = 'M'
