from django.db import models
import uuid

from .unite import Unite
from .version import Version


class Composant(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)


class VersionComposant(Version):
    libelle = models.CharField(max_length=50)
    prix = models.DecimalField(max_digits=19, decimal_places=2)
    unite = models.CharField(choices=Unite.choices, max_length=50)
    composant = models.ForeignKey(Composant, on_delete=models.CASCADE)
