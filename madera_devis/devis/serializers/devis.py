from django.db.models import Q
from rest_framework import serializers

from devis.models import Devis, LigneDevis, VersionModule, Gamme, Module, VersionGamme


class GammeSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source='gamme.uuid')

    class Meta:
        model = VersionGamme
        fields = ('uuid', 'libelle', 'finition_exterieure', 'type_isolant', 'type_couverture')
        read_only_fields = ('libelle', 'finition_exterieure', 'type_isolant', 'type_couverture')


class LigneDevisSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source='module.uuid')
    libelle = serializers.SerializerMethodField()
    prix = serializers.SerializerMethodField()

    class Meta:
        model = LigneDevis
        fields = ('uuid', 'libelle', 'quantite', 'prix')
        read_only_fields = ('libelle', 'prix')

    def get_libelle(self, obj):

        if obj.devis.date_emission is not None:
            libelle = VersionModule.objects \
                .filter(Q(module=obj.module) & Q(date__lte=obj.devis.date_emission)) \
                .latest('date') \
                .libelle
        else:
            libelle = VersionModule.objects \
                .filter(Q(module=obj.module)) \
                .latest('date') \
                .libelle

        return libelle

    def get_prix(self, obj):
        if obj.devis.date_emission is not None:
            module = VersionModule.objects \
                .filter(Q(module=obj.module) & Q(date__lte=obj.devis.date_emission)) \
                .latest('date')
        else:
            module = VersionModule.objects \
                .filter(Q(module=obj.module)) \
                .latest('date')

        return module.prix


class DevisSerializer(serializers.ModelSerializer):
    # Dans la requête, on ne reçoit que l'UUID de la gamme
    gamme = serializers.CharField()
    prix = serializers.DecimalField(max_digits=19, decimal_places=2, read_only=True)
    modules = LigneDevisSerializer(many=True, source='lignedevis_set')

    class Meta:
        model = Devis
        fields = (
            'uuid', 'commercial', 'reference_projet', 'nom_projet', 'client', 'date_emission', 'etat', 'est_modele',
            'marge_commerciale', 'marge_entreprise', 'commentaires', 'gamme', 'prix', 'modules')
        read_only_fields = (
            'date_emission', 'etat', 'est_modele', 'marge_commerciale', 'marge_entreprise', 'commentaires', 'prix')

    def create(self, validated_data):
        gamme_uuid = validated_data.pop('gamme')
        # dans validated_data, l'attribut est nommé comme le nom originel de la propriété du modèle et non comme le nom
        # défini dans le serializer
        modules_data = validated_data.pop('lignedevis_set')
        devis = Devis.objects.create(**validated_data)

        for module_data in modules_data:
            gamme = Gamme.objects.get(uuid=gamme_uuid)
            LigneDevis.objects.create(gamme=gamme,
                                      module=Module.objects.get(uuid=module_data.get('module').get('uuid')),
                                      devis=devis,
                                      quantite=module_data.get('quantite'))
        return devis

    # Dans la réponse, on renvoie tous les attributs de la version de gamme associée au devis
    def to_representation(self, obj):
        self.fields['gamme'] = GammeSerializer()
        return super().to_representation(obj)


class DevisAccepteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Devis
        fields = '__all__'
        read_only_fields = (
            'commercial', 'reference_projet', 'nom_projet', 'client', 'date_emission', 'etat', 'est_modele',
            'marge_commerciale', 'marge_entreprise', 'commentaires')

    def update(self, instance, validated_data):
        instance.accepter()
        instance.save()
        return instance


class DevisRefuseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Devis
        fields = '__all__'
        read_only_fields = (
            'commercial', 'reference_projet', 'nom_projet', 'client', 'date_emission', 'etat', 'est_modele',
            'marge_commerciale', 'marge_entreprise', 'commentaires')

    def update(self, instance, validated_data):
        instance.refuser()
        instance.save()
        return instance


class DevisCommenteSerializer(serializers.ModelSerializer):
    pass
