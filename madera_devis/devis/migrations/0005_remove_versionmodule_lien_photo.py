# Generated by Django 3.1.5 on 2021-01-25 23:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devis', '0004_auto_20210125_0002'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='versionmodule',
            name='lien_photo',
        ),
    ]
