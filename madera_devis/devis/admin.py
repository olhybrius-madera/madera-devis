from django.contrib import admin

from .models import Devis, Composant, Client, Gamme, LigneDevis, Module, PieceJointe, VersionGamme, \
    VersionComposant, VersionModule
from .models.module import Composition

admin.site.register(Devis)
admin.site.register(Client)
admin.site.register(Composant)
admin.site.register(Gamme)
admin.site.register(LigneDevis)
admin.site.register(Module)
admin.site.register(PieceJointe)
admin.site.register(VersionGamme)
admin.site.register(VersionComposant)
admin.site.register(VersionModule)
admin.site.register(Composition)
