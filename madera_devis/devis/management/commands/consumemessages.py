import json
from datetime import datetime

from django.core.management.base import BaseCommand, CommandError
import pika
from django.conf import settings
from socket import gaierror
import time

from django.db.models import Q

from devis.models import Module, VersionModule, Composant, VersionComposant, Gamme, VersionGamme, Client
from devis.models.module import Composition


class Command(BaseCommand):
    help = 'RabbitMQ consumer example for Madera app'

    def handle(self, *args, **options):
        credentials = pika.PlainCredentials(settings.BROKER_USER, settings.BROKER_PASSWORD)
        parameters = pika.ConnectionParameters(settings.BROKER_HOST,
                                               settings.BROKER_PORT,
                                               settings.BROKER_VHOST,
                                               credentials)
        subscriptions = settings.SUBSCRIPTIONS.split(',')

        while True:
            try:
                connection = pika.BlockingConnection(parameters)
                channel = connection.channel()
                channel.exchange_declare(exchange='madera', exchange_type='topic', durable=True)
                channel.queue_declare(queue=settings.BROKER_QUEUE, durable=True)

                print('sub', subscriptions)
                for subscription in subscriptions:
                    channel.queue_bind(exchange='madera', queue=settings.BROKER_QUEUE, routing_key=subscription)

                print(' [*] Waiting for messages. To exit press CTRL+C')
                channel.basic_consume(queue=settings.BROKER_QUEUE, on_message_callback=self.callback)

                channel.start_consuming()
            except (pika.exceptions.ConnectionClosed, pika.exceptions.AMQPConnectionError, gaierror):
                print('Broker unreachable, retrying to connect in', settings.BROKER_CONNECTION_RETRY_INTERVAL,
                      'seconds')
                time.sleep(settings.BROKER_CONNECTION_RETRY_INTERVAL)
                continue
            except KeyboardInterrupt:
                channel.stop_consuming()

            connection.close()
            break

    def callback(self, ch, method, properties, body):
        print(" [x] %r:%r" % (method.routing_key, body))
        payload = json.loads(body)
        date_version = datetime.now()

        if method.routing_key in ['composant.cree', 'composant.maj']:
            uuid = payload.pop('uuid')
            composant, created = Composant.objects.get_or_create(uuid=uuid)
            VersionComposant.objects.create(composant=composant, date=date_version, **payload)
            compositions_impactees = Composition.objects\
                .filter(composant=composant)\
                .order_by('module__uuid', '-date')\
                .distinct('module__uuid')

            for composition in compositions_impactees:
                # Création d'une nouvelle version avec les valeurs des champs de la version précédente copiées
                composition.pk = None
                composition.id = None
                composition.save()
                composition.date = date_version
                composition.save()

                module_impacte = composition.module
                version_precedente_module = VersionModule.objects.filter(module=module_impacte).latest('date')
                # Création d'une nouvelle version avec les valeurs des champs de la version précédente copiées
                version_precedente_module.pk = None
                version_precedente_module.id = None
                version_precedente_module.save()
                version_precedente_module.date = date_version
                version_precedente_module.save()

        if method.routing_key in ['module.cree', 'module.maj']:
            uuid = payload.pop('uuid')
            gamme = payload.pop('gamme')
            composants = payload.pop('composants')

            module, created = Module.objects.get_or_create(uuid=uuid)
            VersionModule.objects.create(module=module, gamme_id=gamme, date=date_version, **payload)

            composition_precedente = module.composition_set \
                .filter(date__lte=date_version) \
                .order_by('composant__uuid', '-date') \
                .distinct('composant__uuid')

            for composant in composants:

                if not composition_precedente \
                        .filter(Q(composant_id=composant['uuid']) & Q(quantite=composant['quantite'])).exists():
                    Composition.objects.create(module=module, composant_id=composant['uuid'],
                                               quantite=composant['quantite'], date=date_version)

        if method.routing_key in ['gamme.cree', 'gamme.maj']:
            uuid = payload.pop('uuid')
            modules = payload.pop('modules')

            gamme, created = Gamme.objects.get_or_create(uuid=uuid)
            VersionGamme.objects.create(gamme=gamme, date=date_version, **payload)

            for module in modules:
                version_precedente = VersionModule.objects.filter(module_id=module).latest('date')
                # Contrairement au filter, on doit convertir l'UUID en string car Python ne gère pas la conversion
                # automatiquement, alors que Postgre oui
                if str(version_precedente.gamme_id) != uuid:
                    # Création d'une nouvelle version avec les valeurs des champs de la version précédente copiées
                    version_precedente.pk = None
                    version_precedente.id = None
                    version_precedente.save()

                    version_precedente.gamme = gamme
                    version_precedente.date = date_version
                    version_precedente.save()

        if method.routing_key == 'client.cree':
            uuid = payload.pop('uuid')
            client, created = Client.objects.get_or_create(uuid=uuid, **payload)
            client.save()

        if method.routing_key == 'client.maj':
            uuid = payload.pop('uuid')
            Client.objects.filter(uuid=uuid).update(**payload)

        ch.basic_ack(delivery_tag=method.delivery_tag)
